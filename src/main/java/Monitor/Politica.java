package Monitor;

import cern.colt.list.AbstractDoubleList;
import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.impl.DenseDoubleMatrix1D;
import cern.colt.matrix.impl.DenseDoubleMatrix2D;
import cern.colt.matrix.linalg.Algebra;

import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;

public class Politica {
    private DoubleMatrix2D matrizDePrioridades;
    private double[] vectorPrioridades;

    public Politica(int[] vectorDePrioridades) {
        this.vectorPrioridades = new double[vectorDePrioridades.length];
        for (int i = 0; i < vectorDePrioridades.length; i++) {
            this.vectorPrioridades[i] = vectorDePrioridades[i];
        }
        this.matrizDePrioridades = hacerMatrizDePrioridades(this.vectorPrioridades);
    }

    public Politica(double[] vectorDePrioridades) {
        this.vectorPrioridades = vectorDePrioridades;
        this.matrizDePrioridades = hacerMatrizDePrioridades(vectorDePrioridades);
    }

    public Politica(AbstractDoubleList vectorDePrioridades) {
        this.vectorPrioridades = vectorDePrioridades.elements();
        this.matrizDePrioridades = hacerMatrizDePrioridades(vectorDePrioridades.elements());
    }

    public Politica(String propertyFile) {
        this.vectorPrioridades = readPoliticasFromPropertiesFile(propertyFile);
        this.matrizDePrioridades = hacerMatrizDePrioridades(vectorPrioridades);
    }

    private DoubleMatrix2D hacerMatrizDePrioridades(double[] vectorDePrioridades) {
        int dimension = vectorDePrioridades.length;
        DoubleMatrix2D matrix = new DenseDoubleMatrix2D(dimension, dimension);

        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) {
                if (vectorDePrioridades[i] == j) {
                    matrix.set(i, j, 1);
                } else {
                    matrix.set(i, j, 0);
                }
            }
        }
        return matrix;
    }

    public double getMaximaPrioridad(double[] RdPThreads) {
        DoubleMatrix1D ArrayRdPThreads = new DenseDoubleMatrix1D(RdPThreads);
        Algebra algebra = new Algebra();

        DoubleMatrix1D resultMult = algebra.mult(matrizDePrioridades, ArrayRdPThreads);

        int indexResult = 0;

        for (int i = 0; i < resultMult.size(); i++) {
            if (resultMult.get(i) == 1) {
                indexResult = i;
                break;
            }
        }

        return vectorPrioridades[indexResult];
    }

    public double[] getMaximaPrioridadArray(double[] RdPThreads) {
        double priorityDouble = getMaximaPrioridad(RdPThreads);
        double[] prorityArray = new double[RdPThreads.length];
        for (int i = 0; i < RdPThreads.length; i++) {
            if (priorityDouble == i) {
                prorityArray[i] = 1;
            } else {
                prorityArray[i] = 0;
            }
        }
        return prorityArray;
    }

    public void setPolitica(int[] vectorDePrioridades) {
        double[] doubleArray = new double[vectorDePrioridades.length];
        for (int i = 0; i < vectorDePrioridades.length; i++) {
            doubleArray[i] = vectorDePrioridades[i];
        }
        this.matrizDePrioridades = hacerMatrizDePrioridades(doubleArray);
    }

    public void setPolitica(double[] vectorDePrioridades) {
        this.matrizDePrioridades = hacerMatrizDePrioridades(vectorDePrioridades);
    }

    public void setPolitica(AbstractDoubleList vectorDePrioridades) {
        this.matrizDePrioridades = hacerMatrizDePrioridades(vectorDePrioridades.elements());
    }

    public DoubleMatrix2D getMatrizDePrioridades() {
        return matrizDePrioridades;
    }

    private double[] readPoliticasFromPropertiesFile(String propertyFile) {
        Properties prop = new Properties();
        try {
            prop.load(getClass().getClassLoader().getResourceAsStream(propertyFile));
        } catch (IOException e) {
            e.printStackTrace();
        }

        String[] x = prop.getProperty("politicas").split(",");
        return Arrays.stream(x)
                .mapToDouble(Double::parseDouble)
                .toArray();
    }
}
