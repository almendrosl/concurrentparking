package Monitor;

import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

//Semaforo que permite la ejecucion de un solo hilo y los demas son dormidos en una cola FIFO
public final class Mutex {
    private static final Mutex INSTANCE = new Mutex();

    //private Semaphore semaphore;
    private ReentrantLock lock;

    //Inicializa semáforo en 1 porque es mutex y solo quiero exclusión mutua y además
    //Fairness true para que la cola de espera sea FIFO
    private Mutex() {
        lock = new ReentrantLock();
        //semaphore = new Semaphore(1, true);
    }

    public static Mutex getMutex(){
        return INSTANCE;
    }

    public void acquire(){
        lock.lock();
    }

    public void release(){
        lock.unlock();
    }

    public ReentrantLock getLock() {
        return lock;
    }

    /*
    public void acquire() {
        try {
            semaphore.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void release() {
        semaphore.release();
    }

    public int getQueueLength() {
        return semaphore.getQueueLength();
    }

    public boolean hasQueueThreads() {
        return semaphore.hasQueuedThreads();
    }

    public boolean tryAcquire() {
        return semaphore.tryAcquire();
    }
    */

}
