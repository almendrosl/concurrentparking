package Monitor;

import RdP.RdP;
import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.impl.DenseDoubleMatrix1D;
import simulacion.simulacion.Logger;

import java.util.ArrayList;
import java.util.concurrent.locks.Condition;

public class GestorDeMonitor {
    private final Mutex mutex;
    private int cantidadTransiciones;
    private ArrayList<Condition> colas;
    private Politica politicas;
    private RdP rdp;
    private Logger logger;

    public GestorDeMonitor(int cantidadTransiciones, Politica politicas, RdP rdp, Logger logger) {
        mutex = Mutex.getMutex();
        this.cantidadTransiciones = cantidadTransiciones;
        colas = new ArrayList<Condition>();
        this.politicas = politicas;
        this.rdp = rdp;
        this.logger = logger;
        for(int i = 0; i<cantidadTransiciones; i++)
            colas.add(Mutex.getMutex().getLock().newCondition());
    }

    public void dispararTransicion(int transicion) throws InterruptedException{
        Mutex.getMutex().acquire();
        try{
            while(!rdp.disparar(transicion))
                colas.get(transicion).await();
            logger.logDisparo(transicion);
            double[] sensibilizadas = rdp.sensibilizadas();
            double[] quienesEstan = quienesEstan();
            new DenseDoubleMatrix1D(sensibilizadas);
            double[] addVector = getAddVector(sensibilizadas, quienesEstan);
            int cantidadDeDisparables = cual(addVector);
            if (cantidadDeDisparables == 1) {
                int hiloDespertable = 0;
                for (int i = 0; i < cantidadTransiciones; i++) {
                    if (addVector[i] != 0) {
                        hiloDespertable = i;
                    }
                }
                colas.get(hiloDespertable).signal();
            }
            else if(cantidadDeDisparables > 1){
                double thrreadDespertar = politicas.getMaximaPrioridad(addVector);
                colas.get((int) thrreadDespertar).signal();
            }
        }
        finally {
            Mutex.getMutex().release();
        }

    }

    private double[] getAddVector(double[] sensibilizadas, double[] quienesEstan) {
        DenseDoubleMatrix1D sensibilizadasCOLT = new DenseDoubleMatrix1D(sensibilizadas);
        DenseDoubleMatrix1D quienesEstanCOLT = new DenseDoubleMatrix1D(quienesEstan);
        DoubleMatrix1D mult = sensibilizadasCOLT.assign(quienesEstanCOLT, cern.jet.math.Functions.mult);
        return mult.toArray();
    }

    private int cual(double[] addVector) {
        int cantidadDeDisparables = 0;
        for (int i = 0; i < cantidadTransiciones; i++) {
            if (addVector[i] > 0) {
                cantidadDeDisparables++;
            }
        }
        return cantidadDeDisparables;
    }

    private double[] quienesEstan(){
        double [] colasOcupadas = new double[cantidadTransiciones];
        for(int i = 0; i<cantidadTransiciones;i++){
            if(Mutex.getMutex().getLock().hasWaiters(colas.get(i)))
                colasOcupadas[i] = 1;
            else
                colasOcupadas[i] = 0;
        }
        return colasOcupadas;
    }
}
