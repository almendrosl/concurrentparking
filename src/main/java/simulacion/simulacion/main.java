package simulacion.simulacion;

import Matrix.MatrixHash;
import Matrix.MatrixType;
import Monitor.GestorDeMonitor;
import Monitor.Politica;
import RdP.InvarianteP;
import RdP.PetriNet;
import XMLLector.HTMLReader;
import XMLLector.MatrixParser;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class main {

	private static double[][] ventanas;
	private static List<InvarianteP> Pinvariantes;

	public static void main(String[] args) {
		/* Cargamos los datos que se van a simular. */
		setUp();

		/* Inicializo el Monitor */
		Logger logger = new Logger();
		Politica politicas = new Politica("data.properties");
		PetriNet RdP = new PetriNet(ventanas, logger,Pinvariantes);

		GestorDeMonitor monitor = new GestorDeMonitor(RdP.getTransiciones(), politicas, RdP, logger);
		/* ************************************************************************************** */
		
		/*Creo los hilos de las entradas, los que toman autos y los que salen de la entrada*/
		HiloComun hiloEntrada1In = new HiloComun(new int[]{0},monitor,"Un auto esta en la cola de la entrada 1");
		hiloEntrada1In.setName("hiloEntrada1In");
		HiloComun hiloEntrada1Out = new HiloComun(new int[]{1},monitor,"Un auto esta saliendo de la cola en la entrada 1");
		hiloEntrada1Out.setName("hiloEntrada1Out");
		HiloComun hiloEntrada2In = new HiloComun(new int[]{12},monitor,"Un auto esta en la cola de la entrada 2");
		hiloEntrada2In.setName("hiloEntrada2In");
		HiloComun hiloEntrada2Out = new HiloComun(new int[]{16},monitor,"Un auto esta saliendo de la cola en la entrada 2");
		hiloEntrada2Out.setName("hiloEntrada2Out");
		HiloComun hiloEntrada3In = new HiloComun(new int[]{17},monitor,"Un auto esta en la cola de la entrada 3");
		hiloEntrada3In.setName("hiloEntrada3In");
		HiloComun hiloEntrada3Out = new HiloComun(new int[]{18},monitor,"Un auto esta saliendo de la cola en la entrada 3");
		hiloEntrada3Out.setName("hiloEntrada3Out");

		/* ************************************************************************************** */
		
		/*Creo los hilos del estacionamiento superior e inferior*/
		/*para que primero se llene el piso inferior creo mas hilos para esto*/
		HiloCreador hiloEstacionamientoInf1 = new HiloCreador(new int[]{19},new int[]{20},monitor,"Un auto se esta estacionando en el parking inferior", "Un auto se va del parking inferior");
		HiloCreador hiloEstacionamientoInf2 = new HiloCreador(new int[]{19},new int[]{20},monitor,"Un auto se esta estacionando en el parking inferior", "Un auto se va del parking inferior");
		HiloCreador hiloEstacionamientoInf3 = new HiloCreador(new int[]{19},new int[]{20},monitor,"Un auto se esta estacionando en el parking inferior", "Un auto se va del parking inferior");
		HiloCreador hiloEstacionamientoInf4 = new HiloCreador(new int[]{19},new int[]{20},monitor,"Un auto se esta estacionando en el parking inferior", "Un auto se va del parking inferior");
		HiloCreador hiloEstacionamientoInf5 = new HiloCreador(new int[]{19},new int[]{20},monitor,"Un auto se esta estacionando en el parking inferior", "Un auto se va del parking inferior");
		HiloCreador hiloEstacionamientoInf6 = new HiloCreador(new int[]{19},new int[]{20},monitor,"Un auto se esta estacionando en el parking inferior", "Un auto se va del parking inferior");
		HiloCreador hiloEstacionamientoInf7 = new HiloCreador(new int[]{19},new int[]{20},monitor,"Un auto se esta estacionando en el parking inferior", "Un auto se va del parking inferior");
		HiloCreador hiloEstacionamientoInf8 = new HiloCreador(new int[]{19},new int[]{20},monitor,"Un auto se esta estacionando en el parking inferior", "Un auto se va del parking inferior");
		HiloCreador hiloEstacionamientoInf9 = new HiloCreador(new int[]{19},new int[]{20},monitor,"Un auto se esta estacionando en el parking inferior", "Un auto se va del parking inferior");
		HiloCreador hiloEstacionamientoInf10 = new HiloCreador(new int[]{19},new int[]{20},monitor,"Un auto se esta estacionando en el parking inferior", "Un auto se va del parking inferior");
		hiloEstacionamientoInf1.setName("hiloEstacionamientoInf1");
		hiloEstacionamientoInf2.setName("hiloEstacionamientoInf2");
		hiloEstacionamientoInf3.setName("hiloEstacionamientoInf3");
		hiloEstacionamientoInf4.setName("hiloEstacionamientoInf4");
		hiloEstacionamientoInf5.setName("hiloEstacionamientoInf5");
		hiloEstacionamientoInf6.setName("hiloEstacionamientoInf6");
		hiloEstacionamientoInf7.setName("hiloEstacionamientoInf7");
		hiloEstacionamientoInf8.setName("hiloEstacionamientoInf7");
		hiloEstacionamientoInf9.setName("hiloEstacionamientoInf7");
		hiloEstacionamientoInf10.setName("hiloEstacionamientoInf7");

		int[] disparosEstacionamientoSupIn = {22,2};/*disparos para estacionar el auto*/
		int[] disparosEstacionamientoSupOut = {3,4,5};/*disparos para sacar el auto del estacionamiento y mandarlos a la salida*/

		HiloComun hiloEstacionamientoSup1 = new HiloComun(new int[]{21},monitor,"Un auto se esta estacionando en el parking superior");
		hiloEstacionamientoSup1.setName("hiloEstacionamientoSup1");
		HiloComun hiloEstacionamientoSup2 = new HiloComun(new int[]{21},monitor,"Un auto se esta estacionando en el parking superior");
		hiloEstacionamientoSup2.setName("hiloEstacionamientoSup2");
		HiloComun hiloEstacionamientoSup3 = new HiloComun(new int[]{21},monitor,"Un auto se esta estacionando en el parking superior");
		hiloEstacionamientoSup3.setName("hiloEstacionamientoSup3");
		HiloComun hiloEstacionamientoSup4 = new HiloComun(new int[]{21},monitor,"Un auto se esta estacionando en el parking superior");
		hiloEstacionamientoSup4.setName("hiloEstacionamientoSup4");
		HiloComun hiloEstacionamientoSup5 = new HiloComun(new int[]{21},monitor,"Un auto se esta estacionando en el parking superior");
		hiloEstacionamientoSup5.setName("hiloEstacionamientoSup5");
		HiloComun hiloEstacionamientoSup6 = new HiloComun(new int[]{21},monitor,"Un auto se esta estacionando en el parking superior");
		hiloEstacionamientoSup6.setName("hiloEstacionamientoSup6");
		HiloComun hiloEstacionamientoSup7 = new HiloComun(new int[]{21},monitor,"Un auto se esta estacionando en el parking superior");
		hiloEstacionamientoSup7.setName("hiloEstacionamientoSup7");

		/* Este es el hilo del ascensor que sube a los autos al piso superior. */
		HiloCreador hiloAscensorUp = new HiloCreador(disparosEstacionamientoSupIn,disparosEstacionamientoSupOut, monitor,"Un auto sube por el ascensor al parking superior", "Un auto baja por el ascensor desde el parking superior");
		hiloAscensorUp.setName("hiloAscensorUp");
		/* ************************************************************************************** */
		
		/* Creo los hilos de las salidas */
		int[] salida1 = {6,7,8};
		int[] salida2 = {9,10,11};
		HiloComun salidaCalle11 = new HiloComun(salida1,monitor,"Un auto esta saliendo por la salida calle 1");
		salidaCalle11.setName("salidaCalle11");
		HiloComun salidaCalle12 = new HiloComun(salida1,monitor,"Un auto esta saliendo por la salida calle 1");
		salidaCalle12.setName("salidaCalle12");
		HiloComun salidaCalle13 = new HiloComun(salida1,monitor,"Un auto esta saliendo por la salida calle 1");
		salidaCalle13.setName("salidaCalle13");

		HiloComun salidaCalle21 = new HiloComun(salida2,monitor,"Un auto esta saliendo por la salida calle 2");
		salidaCalle21.setName("salidaCalle21");
		HiloComun salidaCalle22 = new HiloComun(salida2,monitor,"Un auto esta saliendo por la salida calle 2");
		salidaCalle22.setName("salidaCalle22");
		HiloComun salidaCalle23 = new HiloComun(salida2,monitor,"Un auto esta saliendo por la salida calle 2");
		salidaCalle23.setName("salidaCalle23");
		/* ************************************************************************************** */
		
		/* Creo los hilo del cartel */
		int[] disparosCartel = {13,14,15};
		HiloComun cartel = new HiloComun(disparosCartel,monitor,"Se prendio el cartel");
		cartel.setName("cartel");
		/* ************************************************************************************** */
		
		/* Inicio cada uno de los hilos. Debo comentar los hilos necesarios para implementar aun mejor la Politica.  */
		hiloEntrada1In.start();
		hiloEntrada1Out.start();
		hiloEntrada2In.start();
		hiloEntrada2Out.start();
		hiloEntrada3In.start();
		hiloEntrada3Out.start();
		hiloEstacionamientoInf1.start();
		/*hiloEstacionamientoInf2.start();
		hiloEstacionamientoInf3.start();
		hiloEstacionamientoInf4.start();
		hiloEstacionamientoInf5.start();
		hiloEstacionamientoInf6.start();
		hiloEstacionamientoInf7.start();
		hiloEstacionamientoInf8.start();
		hiloEstacionamientoInf9.start();
		hiloEstacionamientoInf10.start();*/
		hiloEstacionamientoSup1.start();
		hiloEstacionamientoSup2.start();
		hiloEstacionamientoSup3.start();
		hiloEstacionamientoSup4.start();
		hiloEstacionamientoSup5.start();
		hiloEstacionamientoSup6.start();
		hiloEstacionamientoSup7.start();
		hiloAscensorUp.start();
		salidaCalle11.start();
		//salidaCalle12.start();
		//salidaCalle13.start();
		salidaCalle21.start();
		salidaCalle22.start();
		salidaCalle23.start();
		cartel.start();
		/* ************************************************************************************** */

		/* Debo dormir un tiempo x para dejar que se ejecuten los hilos sin problemas*/
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		/* Seteo los hilos, para que la siguiente ejecucion del bucle sea la ultima (pueden quedar hilos en el monitor).
		*/

		hiloEntrada1In.finalizar();
		hiloEntrada2In.finalizar();
		hiloEntrada3In.finalizar();


		/* Dejamos un tiempo para que el resto de los hilos puedan terminar asi de esta manera volvemos al estado
		* incial del sistema.
		*/
		System.out.println("***Cerrando las entradas***");
		try {
			Thread.sleep(50000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		hiloEntrada1Out.finalizar();
		hiloEntrada2Out.finalizar();
		hiloEntrada3Out.finalizar();
		hiloEstacionamientoInf1.finalizar();
		hiloEstacionamientoInf2.finalizar();
		hiloEstacionamientoInf3.finalizar();
		hiloEstacionamientoInf4.finalizar();
		hiloEstacionamientoInf5.finalizar();
		hiloEstacionamientoInf6.finalizar();
		hiloEstacionamientoInf7.finalizar();
		hiloEstacionamientoInf8.finalizar();
		hiloEstacionamientoInf9.finalizar();
		hiloEstacionamientoInf10.finalizar();
		hiloEstacionamientoSup1.finalizar();
		hiloEstacionamientoSup2.finalizar();
		hiloEstacionamientoSup3.finalizar();
		hiloEstacionamientoSup4.finalizar();
		hiloEstacionamientoSup5.finalizar();
		hiloEstacionamientoSup6.finalizar();
		hiloEstacionamientoSup7.finalizar();
		hiloAscensorUp.finalizar();
		salidaCalle11.finalizar();
		salidaCalle12.finalizar();
		salidaCalle13.finalizar();
		salidaCalle21.finalizar();
		salidaCalle22.finalizar();
		salidaCalle23.finalizar();
		cartel.finalizar();

		System.out.println("***Cerrando el estacionamiento***");

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/* ************************************************************************************** */
		
		/* Luego de esperar un tiempo para que termine la ejecucion de todos los hilos, ordeno los logs realizados y los
		* escribo en un archivo.*/

		logger.sortLogs();

		try {
			logger.createFile();
		}
		catch (IOException e){
			e.printStackTrace();
		}

		try {
			logger.createFileWithTime();
		}
		catch (IOException e){
			e.printStackTrace();
		}

		RdP.showMarcado();

		/* Termino el programa. */
		System.exit(0);

	}

	private static void setUp() {
		/* Cargo las matrices. */
		File file = new HTMLReader("matrices.html").getFile();
		new MatrixParser(file);
		int transiciones = MatrixHash.getMatrixHash().getMatrixDataType(MatrixType.COMBINED_INCIDENCE)[0].length;
        Ventana vent = new Ventana("data.properties");
		ventanas = new double[transiciones][2];
		ventanas = vent.getVentanas(ventanas);
		InvariantesPFactory invariantesP = new InvariantesPFactory("data.properties");
		Pinvariantes = invariantesP.getInvariantes();
	}
}
