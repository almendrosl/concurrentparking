package simulacion.simulacion;

import Monitor.GestorDeMonitor;

public class HiloCreador extends Hilo{

    private String tareaHijo;
    private int[] disparosHijo;

    /**
     * Hilo que hace su secuencia de disparos y despues crea un hijo y lo hace disparar tambien.
     * @param vectorDeDisparos Transiciones que tiene disparar.
     * @param vectorHijo Transiciones que tiene disparar el hijo.
     * @param monitor Monitor para asegurar la concurrencia.
     * @param tareaFinalizada Mensaje que imprime cuando termina su secuencia de disparos.
     * @param tareaHijo Mensaje que tiene que imprimir cuando termina su secuencia de disparos.
     */
    public HiloCreador(int vectorDeDisparos[], int vectorHijo[], GestorDeMonitor monitor, String tareaFinalizada, String tareaHijo){
        super(vectorDeDisparos , monitor, tareaFinalizada);
        this.tareaHijo = tareaHijo;
        this.disparosHijo = vectorHijo;
        seguir = true;	
    }

    /**
     * Hasta que no es finalizado no para de disparar.
     */
    @Override
    public void run() 
    {        
        while(seguir){
        	disparar();
        }      
    }

    /**
     * Toma el monitor cada vez que quiere hacer un disparo. Cada vez que logra se duerme antes de tirar la siguiente.
     * Al finalizar la secuencia, crea un hijo e imprime un mensaje que detalla el proceso que acaba de ejecutar.
     */
    @Override
	public void disparar(){
		for(int i=0; i<cantDisparos; i++){
            try {
                monitor.dispararTransicion(vectorDisparos[i]);
            }
            catch (InterruptedException e){
                System.out.printf("Soy el hilo" + Thread.currentThread().getId());
            }

            try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
        System.out.println(tareaFinalizada);
		crearHilo();
	}

    /**
     * Deja de disparar.
     */
    @Override
    public void finalizar() {
        seguir = false;
    }

    /**
     * Crea un hijo y lo corre.
     */
    public void crearHilo() {
		HiloShort h = new HiloShort(disparosHijo, monitor,tareaHijo);
		h.run();
	}
}
