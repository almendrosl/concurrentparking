package simulacion.simulacion;

import Monitor.*;

public class HiloComun extends Hilo {

    /**
     * Constructor del hilo que realiza sus disparos hasta que es finalizado.
     * @param vectorDeDisparos Seccuencia de transiciones a disparar.
     * @param monitor Monitor necesario para asegurar la concurrencia.
     * @param tareaFinalizada
     */
    public HiloComun(int[] vectorDeDisparos, GestorDeMonitor monitor, String tareaFinalizada){
        super(vectorDeDisparos, monitor, tareaFinalizada);
        seguir = true;
    }

    /**
     * Hasta que no es finalizado no para de disparar.
     */
    @Override
    public void run()
    {        
        while(seguir){
        	disparar();
        }      
    }

    /**
     * Toma el monitor cada vez que quiere hacer un disparo. Cada vez que logra se duerme antes de tirar la siguiente.
     * Al finalizar la secuencia, imprime un mensaje que detalla el proceso que acaba de ejecutar.
     */
	@Override
    public void disparar(){
		for(int i=0; i<cantDisparos; i++){
		    try {
                monitor.dispararTransicion(vectorDisparos[i]);
            }
		    catch (InterruptedException e){
		        System.out.printf("Soy el hilo" + Thread.currentThread().getId());
            }

            try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
        System.out.println(tareaFinalizada);
	}

    /**
     * Deja de disparar.
     */
    @Override
	public void finalizar() {
		seguir = false;
	}
}
