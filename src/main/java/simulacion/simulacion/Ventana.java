package simulacion.simulacion;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class Ventana {

    private HashMap<Integer,double[]> ventanasList;

    public Ventana(String propertyFile) { loadVentana(propertyFile); }

    private void loadVentana(String propertyFile) {
        Properties prop = new Properties();

        try {
            prop.load(getClass().getClassLoader().getResourceAsStream(propertyFile));
        } catch (IOException e) {
            e.printStackTrace();
        }

        String[] x = prop.getProperty("ventanas").split("\\|");

        ventanasList = new HashMap<Integer,double[]>();
        for (String s : x) {
            String[] vent = s.split(",");
            ventanasList.put(Integer.parseInt(vent[0]),new double[]{Double.parseDouble(vent[1]),Double.parseDouble(vent[2])});
        }
    }

    public double[][] getVentanas(double[][] ventanas) {
        for(Map.Entry<Integer, double[]> entry : ventanasList.entrySet())
            ventanas[entry.getKey()] = entry.getValue();

        return ventanas;
    }

    @Override
    public String toString() {
        String string = "Ventanas: \n";

        for(Map.Entry<Integer, double[]> entry : ventanasList.entrySet())
            string += "" + entry.getKey() + entry.getValue();
        return string;
    }
}
