package simulacion.simulacion;

import Monitor.GestorDeMonitor;

public abstract class Hilo extends Thread {
    int[] vectorDisparos;
    int cantDisparos;
    GestorDeMonitor monitor;
    String tareaFinalizada;
    boolean seguir = true;

    public Hilo(int[] vectorDeDisparos, GestorDeMonitor monitor, String tareaFinalizada){
        this.vectorDisparos = vectorDeDisparos;
        this.cantDisparos = vectorDeDisparos.length;
        this.tareaFinalizada = tareaFinalizada;
        this.monitor = monitor;
    }

    public abstract void run();

    public abstract void disparar();

    public abstract void finalizar();


}
