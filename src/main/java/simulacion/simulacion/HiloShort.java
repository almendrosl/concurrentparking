package simulacion.simulacion;

import Monitor.GestorDeMonitor;

public class HiloShort extends Hilo {

    /**
     * Constructor del hilo corto que solo dispara una vez.
     * @param vectorDeDisparos Transiciones que debe disparar.
     * @param monitor Monitor para asegurar la concurrencia.
     * @param tareaFinalizada Mensaje que imprime al finalizar su tarea.
     */
    public HiloShort(int[] vectorDeDisparos, GestorDeMonitor monitor, String tareaFinalizada){
        super(vectorDeDisparos,monitor,tareaFinalizada);
    }

    /**
     * Espera un tiempo determinado, simulando que esta desarrollando una tarea y despues realiza su disparo.
     */
    @Override
    public void run()
    {
		try {
			Thread.sleep(50);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        disparar();
    }

    /**
     * Dispara las transiciones necesarias condicionado por el monitor. Entre cada una de ellas duerme simulando que
     * hace una tarea. Cuando termina imprime un mensaje que señaliza que acaba de hacer.
     */
    @Override
	public void disparar(){
		for(int i=0; i<cantDisparos; i++){
            try {
                monitor.dispararTransicion(vectorDisparos[i]);
            }
            catch (InterruptedException e){
                System.out.printf("Soy el hilo" + Thread.currentThread().getId());
            }
            try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
		System.out.println(tareaFinalizada);
	}

    @Override
    public void finalizar() { }
}

