package simulacion.simulacion;

import RdP.InvarianteP;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class InvariantesPFactory {
    private List<InvarianteP> invariantes;

    public InvariantesPFactory(String propertyFile) {
        invariantes = readPInvariants(propertyFile);
    }

    private List<InvarianteP> readPInvariants(String propertyFile) {
        Properties prop = new Properties();
        try {
            prop.load(getClass().getClassLoader().getResourceAsStream(propertyFile));
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<InvarianteP> invariantes = new ArrayList<>();
        String[] x = prop.getProperty("invariantesP").split("\\|");

        for (String s : x) {
            InvarianteP inv = new InvarianteP();
            String[] vent = s.split(",");
            inv.setName(vent[0]);
            String[] plazas = vent[1].split("-");
            inv.setPlazas(Arrays.stream(plazas)
                    .mapToInt(Integer::parseInt)
                    .toArray());
            inv.setInvariante(Double.parseDouble(vent[2]));
            invariantes.add(inv);
        }

        return invariantes;
    }

    public List<InvarianteP> getInvariantes() {
        return invariantes;
    }

    public void setInvariantes(List<InvarianteP> invariantes) {
        this.invariantes = invariantes;
    }
}
