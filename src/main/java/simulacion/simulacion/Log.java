
package simulacion.simulacion;

public class Log {
    private String name;
    private long time;

    /**
     * Log que describe la informacion de lso disparos de los hilos.
     * @param name Transicion disparada.
     * @param time Tiempo en milisegundos en el que fue disparada.
     */
    public Log(String name, long time){
        this.name = name;
        this.time = time;
    }

    public String getName() { return name; }

    public long getTime() { return time; }
}
