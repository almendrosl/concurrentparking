package simulacion.simulacion;

import java.io.*;
import java.util.*;

public class Logger {

    private ArrayList<Log> logs;

    public Logger(){
        logs = new ArrayList<Log>();
    }

    /**
     * Metodo que ordena los logs segun el timestamp.
     */
    public void sortLogs() {
        Comparator<Log> valueComparator = new Comparator<Log>() {

            @Override
            public int compare(Log e1, Log e2) {
                Long v1 = e1.getTime();
                Long v2 = e2.getTime();
                return v1.compareTo(v2);
            }
        };

        Collections.sort(logs, valueComparator);

    }

    /**
     * Metodo que utiliza cada hilo para logear la transicion que disparo y el tiempo en que lo hizo.
     * @param transicion transicion que fue disparada.
     */
    public void logDisparo(int transicion){

        Log disparo = new Log("" + codingTransition(transicion),System.currentTimeMillis());
        //Log disparo = new Log("T" + transicion,System.currentTimeMillis());
        //System.out.println(disparo.getName() + " - "+ disparo.getTime());
        logs.add(disparo);
    }

    /**
     * Crea un archivo con un string que contiene la secuencia ordenada de las transiciones que se dispararon.
     * @throws IOException
     */
    public void createFile() throws IOException {

        FileOutputStream outputStream = new FileOutputStream("log_disparos.txt");
        String str = "";
        for(Log e : logs)
            str += e.getName();

        byte[] strToBytes = str.getBytes();

        outputStream.write(strToBytes);

        outputStream.close();
        System.out.println("Archivo de logs.");

    }

    /**
     * Crea un archivo que muestra toda la informacion de los logs.
     * @throws IOException
     */
    public void createFileWithTime() throws IOException{
        FileWriter fileWriter = new FileWriter("log_with_time.txt");
        PrintWriter printWriter = new PrintWriter(fileWriter);

        for(Log e : logs)
            printWriter.println(e.getName() + " - " + e.getTime());

        printWriter.close();
        System.out.println("Archivo de logs con tiempo.");
    }
    public ArrayList<Log> getLog(){
    	return logs;
    }

    private char codingTransition(int transicion){
        return (char)(transicion + 65);
    }


}
