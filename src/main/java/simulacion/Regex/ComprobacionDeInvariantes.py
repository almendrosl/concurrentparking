import re
import os
from pathlib import Path

###############################################################
###############################################################
##Lectura del archivo donde se guardo la secuencia de disparos
def readingSecuencia (archivo):
    script_location = Path(__file__).absolute().parent.parent.parent.parent.parent.parent
    script_location = script_location / archivo
    f = script_location.open()
    linea = []
    for newline in f:
        linea.append(newline)
    f.close()
    return linea

###############################################################
###############################################################

def orden (regex,regex2,secuenciaFinal):
    divicion = []
    ordenEntradaPisos = []
    estacionamientoInferior = []
    cantEstacionamientoInferior = 0
    indexInferior = 0
    while (True):
        estacionamientoInferior.append(cantEstacionamientoInferior)
        divicion = re.split(regex,secuenciaFinal,1)
        for indexRegex in range(len(regex2)):
            estacionamientosuperior = len(re.findall(regex2[indexRegex],divicion[0]))
            ordenEntradaPisos.append(estacionamientosuperior)
        if len(divicion) > 1: 
            secuenciaFinal = divicion[1]
        else:
            break
        if estacionamientosuperior != 0:
            estacionamientoInferior[indexInferior] = estacionamientoInferior[indexInferior] + 1
            indexInferior = indexInferior + 1 
        else:
            estacionamientoInferior[indexInferior] = estacionamientoInferior[indexInferior] + 1 
        flag = False
    ordenEntradaPisos.append(estacionamientoInferior)
    return ordenEntradaPisos
        

###############################################################
###############################################################
# me retorna dos strings, el primero con las tranciciones que no pertenecen a una invariante, y la segunda son
# todas las invariantes ordenadas para poder procesarlas y sacar informacion posteriormente
def Busqueda(regex,secuenciaFinal,substituir,substituir2):
    ResiduosYLog = []
    cortado = []
    ordenado = ''
    # si al finalizar la ejecucion quedan tranciciones en el string, entonces este valor es false
    seCumple = False
    #busco las invariantes hasta que no encunetre mas
    seguirBuscando = True
    invarDisparadas = ''
    while seguirBuscando:
        seguirBuscando = False #si no encuentra ninguno termina el ciclo

        #busco cada uno de las invariantes

        lenBefore = len(secuenciaFinal)

        #log = re.search(regex,secuenciaFinal)

        cortado = re.split(regex,secuenciaFinal,1)
        secuenciaFinal = re.sub(regex,substituir,secuenciaFinal,1,re.MULTILINE)
        if(lenBefore != len(secuenciaFinal)):
            #como encontro uno debe seguir con el ciclo
            seguirBuscando = True
            ordenado = ordenado + re.sub(regex,substituir2,cortado[1],1,re.MULTILINE)
        
        #print(busqueda)
        #print(secuenciaFinal)
    #recorro el vector del tamaño de las invariantes y le resto 1, para ver si quedo algun "fragmento"
    #de invariante
            
    #si al resultado le sigue quedando trancicions, entonces se retorna false, en el caso que todo saliera bien
    #y no hay tranciciones en el resultado se retorna true
    ResiduosYLog.append(secuenciaFinal)
    ResiduosYLog.append(ordenado)
    return ResiduosYLog

############################################################################################################################################################################################################################################################
############################################################################################################################################################################################################################################################

                                ####################
                                ###     Main     ###
                                ####################

#nombre del archivo donde se guarda la secuencia de disparos de la simulacion
archivo ='log_disparos.txt'
#Exprecion regular
regex = '((?P<G100>A)(?P<G1>.*?)(?P<G101>B)(?P<G2>.*?)((?P<G102>T)(?P<G3>.*?)(?P<G103>U)|(?P<G104>V)(?P<G4>.*?)(?P<G105>W)(?P<G5>.*?)(?P<G106>C)(?P<G6>.*?)(?P<G107>D)(?P<G7>.*?)(?P<G108>E)(?P<G8>.*?)(?P<G109>F))(?P<G9>.*?)((?P<G110>G)(?P<G10>.*?)(?P<G111>H)(?P<G11>.*?)(?P<G112>I)|(?P<G113>J)(?P<G12>.*?)(?P<G114>K)(?P<G13>.*?)(?P<G115>L))|(?P<G116>M)(?P<G14>.*?)(?P<G147>Q)(?P<G15>.*?)((?P<G117>T)(?P<G16>.*?)(?P<G118>U)|(?P<G119>V)(?P<G17>.*?)(?P<G120>W)(?P<G18>.*?)(?P<G121>C)(?P<G19>.*?)(?P<G122>D)(?P<G20>.*?)(?P<G123>E)(?P<G21>.*?)(?P<G124>F))(?P<G22>.*?)((?P<G125>G)(?P<G23>.*?)(?P<G126>H)(?P<G24>.*?)(?P<G127>I)|(?P<G128>J)(?P<G25>.*?)(?P<G129>K)(?P<G26>.*?)(?P<G130>L))|(?P<G131>R)(?P<G27>.*?)(?P<G132>S)(?P<G28>.*?)((?P<G133>T)(?P<G29>.*?)(?P<G134>U)|(?P<G135>V)(?P<G30>.*?)(?P<G136>W)(?P<G31>.*?)(?P<G137>C)(?P<G32>.*?)(?P<G138>D)(?P<G33>.*?)(?P<G139>E)(?P<G34>.*?)(?P<G140>F))(?P<G35>.*?)((?P<G141>G)(?P<G36>.*?)(?P<G142>H)(?P<G37>.*?)(?P<G143>I)|(?P<G144>J)(?P<G38>.*?)(?P<G145>K)(?P<G39>.*?)(?P<G146>L))|((?P<G148>N)(?P<G40>.*?)(?P<G149>O)(?P<G41>.*?)(?P<G150>P)))'
#Grupos de la exprecion regular que se relaciona con los valores intermedios a lo que busco
substituir = '\g<G1>\g<G2>\g<G3>\g<G4>\g<G5>\g<G6>\g<G7>\g<G8>\g<G9>\g<G10>\g<G11>\g<G12>\g<G13>\g<G14>\g<G15>\g<G16>\g<G17>\g<G18>\g<G19>\g<G20>\g<G21>\g<G22>\g<G23>\g<G24>\g<G25>\g<G26>\g<G27>\g<G28>\g<G29>\g<G30>\g<G31>\g<G32>\g<G33>\g<G34>\g<G35>\g<G36>\g<G37>\g<G38>\g<G39>\g<G40>\g<G41>'
#Grupos de la exprecion regular que se relaciona con los valores que busco
substituir2 = '\g<G100>\g<G101>\g<G102>\g<G103>\g<G104>\g<G105>\g<G106>\g<G107>\g<G108>\g<G109>\g<G110>\g<G111>\g<G112>\g<G113>\g<G114>\g<G115>\g<G116>\g<G147>\g<G117>\g<G118>\g<G119>\g<G120>\g<G121>\g<G122>\g<G123>\g<G124>\g<G125>\g<G126>\g<G127>\g<G128>\g<G129>\g<G130>\g<G131>\g<G132>\g<G133>\g<G134>\g<G135>\g<G136>\g<G137>\g<G138>\g<G139>\g<G140>\g<G141>\g<G142>\g<G143>\g<G144>\g<G145>\g<G146>\g<G148>\g<G149>\g<G150>'
# lee la secuencia de disparo guardadas en un archivo
secuenciaFinal = readingSecuencia(archivo)

# Ejecuto la busqueda de los invariantes, este me retorna un primer string con las transiciones que no
#concuerdan con ningun invariante, y otro string que contiene todas las secuencias de invariantes ordenadas
#para poder procesarlas y generar un log con los eventos que se produjeron en la simulacion
#print(secuenciaFinal)
Residuos = Busqueda(regex,secuenciaFinal[0],substituir,substituir2)

ordenEntradaPisos = []
listRegex = ['U','E','V']
ordenEntradaPisos = orden('T',listRegex,secuenciaFinal[0])


print()
#muestra el resto
print('Esto es el sobrante: ',Residuos[0])
netoSup = 0
netoInf = 0

#Un pequeño log con los eventos inportantes que sucedieron en la simulacion
print()
print('Autos que pasaron por: ')
print('                       Entrada 1: ', len(re.findall('AB',Residuos[1])))
print('                       Entrada 2: ', len(re.findall('MQ',Residuos[1])))
print('                       Entrada 3: ', len(re.findall('RS',Residuos[1])))
print()
print('                       Planta alta: ', len(re.findall('VWCDE',Residuos[1])))
print('                       Planta baja: ', len(re.findall('TU',Residuos[1])))
print()
print('                       Calle 1: ', len(re.findall('GHI',Residuos[1])))
print('                       Calle 2: ', len(re.findall('JKL',Residuos[1])))
print()
print('                       Veces que el cartel se prendio: ', len(re.findall('NOP',Residuos[1])))
print()
print()
print()

archvo = 'log_disparos_procesados_python.txt'
script_location = Path(__file__).absolute().parent.parent.parent.parent.parent.parent
script_location = script_location / archvo
file = script_location.open("w")
file.write('Resto: ' + Residuos[0] + os.linesep)
file.write('Autos que pasaron por: '+ os.linesep)
file.write('                       Entrada 1: '+ str(len(re.findall('AB',Residuos[1])))+ os.linesep)
file.write('                       Entrada 2: '+ str(len(re.findall('MQ',Residuos[1])))+ os.linesep)
file.write('                       Entrada 3: '+ str(len(re.findall('RS',Residuos[1]))) + os.linesep + os.linesep)
file.write('                       Planta alta: '+ str(len(re.findall('VWCDE',Residuos[1])))+ os.linesep)
file.write('                       Planta baja: '+ str(len(re.findall('TU',Residuos[1])))+ os.linesep + os.linesep)
file.write('                       Calle 1: '+ str(len(re.findall('GHI',Residuos[1])))+ os.linesep)
file.write('                       Calle 2: '+ str(len(re.findall('JKL',Residuos[1])))+ os.linesep)
file.close()


#\g<G1>\g<G2>\g<G3>\g<G4>\g<G5>\g<G6>\g<G7>\g<G8>\g<G9>\g<G10>\g<G11>\g<G12>\g<G13>\g<G14>\g<G15>\g<G16>\g<G17>\g<G18>\g<G19>\g<G20>\g<G21>\g<G22>\g<G23>\g<G24>\g<G25>\g<G26>\g<G27>\g<G28>\g<G29>\g<G30>\g<G31>\g<G32>\g<G33>\g<G34>\g<G35>\g<G36>\g<G37>\g<G38>\g<G39>

#(A(?P<G1>.*?)B(?P<G2>.*?)(T(?P<G3>.*?)U|V(?P<G4>.*?)W(?P<G5>.*?)C(?P<G6>.*?)D(?P<G7>.*?)E(?P<G8>.*?)F)(?P<G9>.*?)(G(?P<G10>.*?)H(?P<G11>.*?)I|J(?P<G12>.*?)K(?P<G13>.*?)L)|M(?P<G14>.*?)Q(?P<G15>.*?)(T(?P<G16>.*?)U|V(?P<G17>.*?)W(?P<G18>.*?)C(?P<G19>.*?)D(?P<G20>.*?)E(?P<G21>.*?)F)(?P<G22>.*?)(G(?P<G23>.*?)H(?P<G24>.*?)I|J(?P<G25>.*?)K(?P<G26>.*?)L)|R(?P<G27>.*?)S(?P<G28>.*?)(T(?P<G29>.*?)U|V(?P<G30>.*?)W(?P<G31>.*?)C(?P<G32>.*?)D(?P<G33>.*?)E(?P<G34>.*?)F)(?P<G35>.*?)(G(?P<G36>.*?)H(?P<G37>.*?)I|J(?P<G38>.*?)K(?P<G39>.*?)L))

#((?P<G100>A)(?P<G1>.*?)(?P<G101>B)(?P<G2>.*?)((?P<G102>T)(?P<G3>.*?)(?P<G103>U)|(?P<G104>V)(?P<G4>.*?)(?P<G105>W)(?P<G5>.*?)(?P<G106>C)(?P<G6>.*?)(?P<G107>D)(?P<G7>.*?)(?P<G108>E)(?P<G8>.*?)(?P<G109>F))(?P<G9>.*?)((?P<G110>G)(?P<G10>.*?)(?P<G111>H)(?P<G11>.*?)(?P<G112>I)|(?P<G113>J)(?P<G12>.*?)(?P<G114>K)(?P<G13>.*?)(?P<G115>L))|(?P<G116>M)(?P<G14>.*?)(?P<G116>Q)(?P<G15>.*?)((?P<G117>T)(?P<G16>.*?)(?P<G118>U)|(?P<G119>V)(?P<G17>.*?)(?P<G120>W)(?P<G18>.*?)(?P<G121>C)(?P<G19>.*?)(?P<G122>D)(?P<G20>.*?)(?P<G123>E)(?P<G21>.*?)(?P<G124>F))(?P<G22>.*?)((?P<G125>G)(?P<G23>.*?)(?P<G126>H)(?P<G24>.*?)(?P<G127>I)|(?P<G128>J)(?P<G25>.*?)(?P<G129>K)(?P<G26>.*?)(?P<G130>L))|(?P<G131>R)(?P<G27>.*?)(?P<G132>S)(?P<G28>.*?)((?P<G133>T)(?P<G29>.*?)(?P<G134>U)|(?P<G135>V)(?P<G30>.*?)(?P<G136>W)(?P<G31>.*?)(?P<G137>C)(?P<G32>.*?)(?P<G138>D)(?P<G33>.*?)(?P<G139>E)(?P<G34>.*?)(?P<G140>F))(?P<G35>.*?)((?P<G141>G)(?P<G36>.*?)(?P<G142>H)(?P<G37>.*?)(?P<G143>I)|(?P<G144>J)(?P<G38>.*?)(?P<G145>K)(?P<G39>.*?)(?P<G146>L)))

#((?P<G100>A)(?P<G1>.*?)(?P<G101>B)(?P<G2>.*?)((?P<G102>T)(?P<G3>.*?)(?P<G103>U)|(?P<G104>V)(?P<G4>.*?)(?P<G105>W)(?P<G5>.*?)(?P<G106>C)(?P<G6>.*?)(?P<G107>D)(?P<G7>.*?)(?P<G108>E)(?P<G8>.*?)(?P<G109>F))(?P<G9>.*?)((?P<G110>G)(?P<G10>.*?)(?P<G111>H)(?P<G11>.*?)(?P<G112>I)|(?P<G113>J)(?P<G12>.*?)(?P<G114>K)(?P<G13>.*?)(?P<G115>L))|(?P<G116>M)(?P<G14>.*?)(?P<G147>Q)(?P<G15>.*?)((?P<G117>T)(?P<G16>.*?)(?P<G118>U)|(?P<G119>V)(?P<G17>.*?)(?P<G120>W)(?P<G18>.*?)(?P<G121>C)(?P<G19>.*?)(?P<G122>D)(?P<G20>.*?)(?P<G123>E)(?P<G21>.*?)(?P<G124>F))(?P<G22>.*?)((?P<G125>G)(?P<G23>.*?)(?P<G126>H)(?P<G24>.*?)(?P<G127>I)|(?P<G128>J)(?P<G25>.*?)(?P<G129>K)(?P<G26>.*?)(?P<G130>L))|(?P<G131>R)(?P<G27>.*?)(?P<G132>S)(?P<G28>.*?)((?P<G133>T)(?P<G29>.*?)(?P<G134>U)|(?P<G135>V)(?P<G30>.*?)(?P<G136>W)(?P<G31>.*?)(?P<G137>C)(?P<G32>.*?)(?P<G138>D)(?P<G33>.*?)(?P<G139>E)(?P<G34>.*?)(?P<G140>F))(?P<G35>.*?)((?P<G141>G)(?P<G36>.*?)(?P<G142>H)(?P<G37>.*?)(?P<G143>I)|(?P<G144>J)(?P<G38>.*?)(?P<G145>K)(?P<G39>.*?)(?P<G146>L)))




