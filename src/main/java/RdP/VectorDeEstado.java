package RdP;

import Matrix.MatrixHash;
import Matrix.MatrixType;
import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.impl.DenseDoubleMatrix1D;
import cern.colt.matrix.impl.DenseDoubleMatrix2D;
import cern.colt.matrix.tint.impl.DenseIntMatrix1D;
import cern.jet.math.Functions;

import java.util.List;


public class VectorDeEstado {

    private double[] marcado;
    private List<InvarianteP> invariantesP;


    public VectorDeEstado(List<InvarianteP> invariantesP){
        marcado = MatrixHash.getMatrixHash().getMatrixDataType(MatrixType.MARKING)[0];
        this.invariantesP = invariantesP;
        checkInvariantesP();
    }

    public void calculoDeVectorEstado(double[] disparo, VectorSensibilizadas sensibilizadas) {

        sensibilizadas.resetEsperando(disparo);

        /* Calculo del nuevo marcado. */
        DenseDoubleMatrix1D disparoColt = new DenseDoubleMatrix1D(disparo);
        DenseDoubleMatrix1D marcadoColt = new DenseDoubleMatrix1D(marcado);
        DenseDoubleMatrix1D sensibilizadasColt = new DenseDoubleMatrix1D(sensibilizadas.getV());
        DenseDoubleMatrix2D matrixIColt = new DenseDoubleMatrix2D(MatrixHash.getMatrixHash().getMatrixDataType(MatrixType.COMBINED_INCIDENCE));
        /* Marcado nuevo = marcado actual + I * (disparo and transiciones sensibilizadas) */
        DoubleMatrix1D sum = marcadoColt.assign(matrixIColt.zMult(disparoColt.assign(sensibilizadasColt, Functions.mult),marcadoColt.like(marcado.length)), Functions.plus);
        //DoubleMatrix1D sum = marcadoColt.assign(matrixIColt.zMult(disparoColt.assign(sensibilizadasColt, Functions.mult),disparoColt.like(disparo.length)),cern.jet.math.Functions.plus);
        marcado = sum.toArray();

        checkInvariantesP();

        /* Actualizo el vector de transiciones sensibilizadas. */
        sensibilizadas.actualiceSensibilizadoT(marcado, disparo);
    }

    public double[] getMarcado() { return marcado; }

    private void checkInvariantesP(){

        for(InvarianteP invP : invariantesP) {
            int[] plazas = invP.getPlazas();
            double suma = 0;
            for (int i = 0; i < plazas.length; i++)
                suma += marcado[plazas[i]];
            if (suma != invP.getInvariante()) {
                System.out.println("No cumple con el invariante " + invP.getName());
            }
        }
    }
}
