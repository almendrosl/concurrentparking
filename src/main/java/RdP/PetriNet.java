package RdP;

import Matrix.MatrixHash;
import Matrix.MatrixType;
import cern.colt.matrix.impl.DenseDoubleMatrix1D;
import simulacion.simulacion.Logger;

import java.util.List;

public class PetriNet implements RdP {

    private VectorDeEstado vectorDeEstado;
    private VectorSensibilizadas vectorSensibilizadas;
    private int transiciones;
    private double[][] matrizI;


    public PetriNet(double[][] ventanas, Logger logger,List<InvarianteP> invariantesP ){
        vectorDeEstado = new VectorDeEstado(invariantesP);
        vectorSensibilizadas = new VectorSensibilizadas(ventanas);
        matrizI = MatrixHash.getMatrixHash().getMatrixDataType(MatrixType.COMBINED_INCIDENCE);
        transiciones = matrizI[0].length;
    }

    public boolean disparar(int transicion) {
        double[]disparo = new double[transiciones];
        disparo[transicion] = 1;
        if(vectorSensibilizadas.estaSensibilizado(disparo)){
            vectorDeEstado.calculoDeVectorEstado(disparo, vectorSensibilizadas);
            return true;
        }
        else
            return false;
    }

    public void showMarcado(){
        System.out.println("Marcado:" + new DenseDoubleMatrix1D(vectorDeEstado.getMarcado()));
    }

    public double[] sensibilizadas(){
        return vectorSensibilizadas.getV();
    }

    public int getTransiciones() { return transiciones; }
}
