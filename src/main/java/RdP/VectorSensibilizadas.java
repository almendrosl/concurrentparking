package RdP;

import Matrix.MatrixHash;
import Matrix.MatrixType;
import Monitor.Mutex;
import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.impl.DenseDoubleMatrix1D;
import cern.colt.matrix.impl.DenseDoubleMatrix2D;
import cern.colt.matrix.tint.IntMatrix1D;
import cern.colt.matrix.tint.impl.DenseIntMatrix1D;
import cern.jet.math.Functions;
import cern.jet.math.tint.IntFunctions;

public class VectorSensibilizadas {

    private double[] vSensibilizadas;
    private boolean esperando;
    private SensibilizadoConTiempo tiempo;

    /**
     *
     * @param ventanas
     */
    public VectorSensibilizadas(double[][] ventanas){
        vSensibilizadas = new double[MatrixHash.getMatrixHash().getMatrixDataType(MatrixType.COMBINED_INCIDENCE)[0].length];
        tiempo = new SensibilizadoConTiempo(ventanas,vSensibilizadas);
        actualiceSensibilizadoT(MatrixHash.getMatrixHash().getMatrixDataType(MatrixType.MARKING)[0], new double[] {0,0});
        //System.out.println(new DenseDoubleMatrix2D(ventanas));
    }

    /**
     * Determina si la transicion esta sensibilizada con respecto al tiempo. Si lo esta pero se encuentra antes de la
     * ventana de tiempo, se duerme hasta que entre a la ventana.
     * @param disp transicion que se quiere disparar.
     * @return true si se puede disparar, false si no.
     */
    public boolean estaSensibilizado(double[] disp){

        DoubleMatrix1D sensibilizadas = new DenseDoubleMatrix1D(vSensibilizadas);
        DoubleMatrix1D disparo = new DenseDoubleMatrix1D(disp);

        int transicion = getTransicion(disp);


        if(sensibilizadas.zDotProduct(disparo) == 0) /* Esta sensibilizada? */
            return false;

        /* Si esta sensibilizada hay que ver como esta con respecto al tiempo. */
        int[] respuesta = tiempo.testVentanaTiempo(transicion);

        if(respuesta[0] == 0){ /* Esta dentro de la ventana. */
            if(respuesta[1] != -1) /* Si no hay otro hilo esperando */
                return true;
            else
                return false;
        }
        else if(respuesta[0] == -1){ /* Esta antes de la ventana.*/
            Mutex.getMutex().release();
            if(respuesta[1] != -1) { /* Si no hay otro hilo esperando*/
                tiempo.setEsperando(transicion);
                long tiempoSueño = (long)tiempo.getTiempoSensibilizada(transicion) + (long)tiempo.getAlpha(transicion) - System.currentTimeMillis();

                try {
                    Thread.currentThread().sleep(tiempoSueño);
                }
                catch (InterruptedException e)
                {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                Mutex.getMutex().acquire();
                return estaSensibilizado(disp);
            }
            else
                return false;
        }
        else /* Esta despues de la ventana.*/
            return false;

        //return true;
    }

    /**
     * Este metodo actualiza el vector de transiciones sensibilizadas despues de un disparo y setea el timestamp nuevo
     * en cada transicion que cambio su estado.
     * @param marcadoActual es el marcado nuevo que surgio despues del disparo.
     */
    public void actualiceSensibilizadoT(double[] marcadoActual, double[]disparo){

        DenseIntMatrix1D aux = new DenseIntMatrix1D(casting(vSensibilizadas));

        DenseDoubleMatrix1D vectorE = new DenseDoubleMatrix1D(getEVector(marcadoActual));
        DenseDoubleMatrix1D vectorB = new DenseDoubleMatrix1D(getBVector(marcadoActual));
        DoubleMatrix1D and = vectorE.assign(vectorB, Functions.mult);

        vSensibilizadas = and.toArray();

        IntMatrix1D cambios = aux.assign(new DenseIntMatrix1D(casting(vSensibilizadas)), IntFunctions.xor);
        int[] cambiadas = cambios.toArray();
        cambiadas[getTransicion(disparo)] = 1;

        tiempo.setNuevoTimeStamp(cambiadas);
    }

    private double[] getEVector(double[] marcadoActual) {

        DenseDoubleMatrix2D incidencia = new DenseDoubleMatrix2D(MatrixHash.getMatrixHash().getMatrixDataType(MatrixType.COMBINED_INCIDENCE));
        DenseDoubleMatrix1D marcado = new DenseDoubleMatrix1D(marcadoActual);

        double[] vectorE = new double[incidencia.viewRow(0).toArray().length];
        /* result = columna de la Matriz I + el marcado actual
         *  Cada elemento del vector E resulta de multiplicar todos los sign(result[i])
         * */
        for (int i = 0; i < incidencia.viewRow(0).toArray().length; i++) {
            DoubleMatrix1D columna = incidencia.viewColumn(i);
            DoubleMatrix1D result = columna.assign(marcado, Functions.plus);
            vectorE[i] = result.aggregate(Functions.mult, cern.jet.math.Functions.greater(-1));
        }

        return vectorE;
    }

    private double[] getBVector(double[] marcado){

        DenseDoubleMatrix1D ceroVector = new DenseDoubleMatrix1D(marcado);
        DenseDoubleMatrix2D inhibicion = new DenseDoubleMatrix2D(MatrixHash.getMatrixHash().getMatrixDataType(MatrixType.INHIBITION));

        /*El vector Q es un vector binario que tiene un 1 si el elemento del marcado es mayor a 0 y un 0 si no lo es.*/
        ceroVector.assign(cern.jet.math.Functions.greater(0));

        DenseDoubleMatrix1D vectorB = new DenseDoubleMatrix1D(vSensibilizadas.length);

        /* Vector B = cero(Ht * Q) */
        inhibicion.zMult(ceroVector,vectorB,1,1,true);
        vectorB.assign(cern.jet.math.Functions.equals(0));

        return vectorB.toArray();
    }

    public double[] getV(){return vSensibilizadas;}

    public void resetEsperando(double[] disparo){ tiempo.resetEsperando(getTransicion(disparo)); }

    private int getTransicion(double[] disparo){
        int i = 0;
        for( i = 0; i<disparo.length; i++)
            if(disparo[i] == 1)
                break;
        return i;
    }

    private int[] casting(double[] vector){
        int[] result = new int[vector.length];
        for(int i = 0; i<vector.length; i++)
            result[i] = (int)vector[i];
        return result;
    }


}
