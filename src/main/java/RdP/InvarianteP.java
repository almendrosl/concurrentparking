package RdP;

public class InvarianteP {
    private String name;
    private int[] plazas;
    private double invariante;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int[] getPlazas() {
        return plazas;
    }

    public void setPlazas(int[] plazas) {
        this.plazas = plazas;
    }

    public double getInvariante() {
        return invariante;
    }

    public void setInvariante(double invariante) {
        this.invariante = invariante;
    }
}
