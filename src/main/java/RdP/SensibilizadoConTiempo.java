package RdP;

import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.impl.DenseDoubleMatrix1D;
import cern.colt.matrix.impl.DenseDoubleMatrix2D;

public class SensibilizadoConTiempo {

    private double[][] ventanas;
    private double[] tiempoSensibilizada;
    private long[] esperando;

    /**
     * Constructor de Sensibilizado con tiempo. Actualiza su matriz de ventanas con la informacion de la PetriNet,
     * actualiza los tiempos de las transiciones que estan habilitadas y crea un arreglo vacio de los hilos que esperan.
     * @param ventanas es una matriz con la informacion de los alfas y betas.
     * @param sensibilizadas vector de transiciones sensibilizadas.
     */
    public SensibilizadoConTiempo(double[][] ventanas, double[] sensibilizadas){
        this.ventanas = ventanas;
        tiempoSensibilizada = inicializacion(sensibilizadas);
        esperando = new long[sensibilizadas.length];
    }

    private double[] inicializacion(double[] sensibilizadas){
        double tiempoActual = System.currentTimeMillis();
        double[] tiempos = new double[sensibilizadas.length];
        for(int i = 0; i< sensibilizadas.length; i++)
            if(sensibilizadas[i] == 1)
                tiempos[i] = tiempoActual;
        return tiempos;
    }

    /**
     * Actualiza los tiempos de las transiciones recientemente sensibilizadas.
     * @param cambios es un vector que tiene en 1 a las transiciones que fueron modificadas recientemente y un 0 si no
     *                se modifico su estado.
     */
    public void setNuevoTimeStamp(int[] cambios){

        double tiempoActual = System.currentTimeMillis();

        for(int i=0;i<cambios.length;i++)
            if (cambios[i] == 1)
                tiempoSensibilizada[i] = tiempoActual;
    }

    /**
     * Se fija si el disparo esta dentro de la ventana de tiempo del disparo y si esa transicion ya tiene un hilo
     * esperando.
     * @param transicion la transicion que se quiere disparar.
     * @return un arreglo que si en la primer posicion devuelve un -1 es que se esta antes de la ventana, un 0 es que
     *         se esta dentro de la ventana y un 1 es que se esta despues de la ventana. En la segunda posicion pone
     *         un 0 si nadie esta esperando, un 1 si el hilo que esta esperando es el mismo y un -1 si otro hilo esta
     *         esperando.
     */
    public int[] testVentanaTiempo(int transicion){

        double tiempoActual = System.currentTimeMillis();

        int[] respuesta = new int[2];


        double tiempoTotal = tiempoActual - tiempoSensibilizada[transicion];

        if (ventanas[transicion][0] == 0 && ventanas[transicion][1] == 0) // La transicion no tiene limitacion temporal.
            return new int[] {0,0};

        if (getAlpha(transicion) > tiempoTotal)
            respuesta[0] = -1; // Antes de la Ventana.
        else if (tiempoTotal < getBeta(transicion))
            respuesta[0] = 0; // En de la Ventana.
        else
            respuesta[0] = 1; // Despues de la Ventana.

        if(esperando[transicion] == 0)
            respuesta[1] = 0; // Nadie esta esperando.
        else if(esperando[transicion] == Thread.currentThread().getId())
            respuesta[1] = 1; // El hilo que esta esperando es este.
        else
            respuesta[1] = -1; // Otro hilo esta esperando.

        return respuesta;
    }

    /**
     * Anota el id del hilo que se va a esperar.
     * @param transicion la transicion que se quiere disparar.
     */
    public void setEsperando(int transicion){ esperando[transicion] = Thread.currentThread().getId(); }

    /**
     * Borra el id del hilo que estaba esperando.
     * @param transicion la transicion que se quiere disparar.
     */
    public void resetEsperando(int transicion){ esperando[transicion] = 0; }

    public double getTiempoSensibilizada(int transicion) { return tiempoSensibilizada[transicion]; }

    public double getAlpha(int transicion){ return ventanas[transicion][0];}

    public double getBeta(int transicion){ return ventanas[transicion][1];}

}
