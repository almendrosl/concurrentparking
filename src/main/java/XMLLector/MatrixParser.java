package XMLLector;

import Matrix.MatrixHash;
import Matrix.MatrixType;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;

public class MatrixParser {

    public MatrixParser(File file) {
        try {
            MatrixHash matrix = MatrixHash.getMatrixHash();
            Document doc = Jsoup.parse(file, "UTF-8");
            Elements todasLasMatrices = doc.select("body>table");
            for (Element matriz : todasLasMatrices) {
                int cantidadFilas = matriz.select("tbody tbody tr").size() - 1;
                int cantidadColumnas = matriz.select("tbody tbody tr:nth-child(2) td.cell").size();
                double[][] matrixTemp = new double[cantidadFilas][cantidadColumnas];

                Elements filas = matriz.select("tbody tbody tr .cell");
                String tableName = matriz.select("td.colhead").first().text();
                int nextValue = 0;
                for (int i = 0; i < cantidadFilas; i++) {
                    for (int j = 0; j < cantidadColumnas; j++) {
                        try {
                            matrixTemp[i][j] = Integer.parseInt(filas.get(nextValue).text());
                        } catch (NumberFormatException e) {
                            matrixTemp[i][j] = filas.get(nextValue).text().matches("yes") ? 1 : 0;
                        }
                        nextValue++;
                    }
                }

                matrix.setMatrixData(MatrixType.getMatrixType(tableName), matrixTemp);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
