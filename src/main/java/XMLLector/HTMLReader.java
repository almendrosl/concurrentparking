package XMLLector;

import java.io.File;
import java.net.URL;

public class HTMLReader {
    private File file;

    public HTMLReader(String fileName) {
        setFile(fileName);
    }

    public File getFile() {
        return this.file;
    }

    public void setFile(String fileName) throws IllegalArgumentException{
        ClassLoader classLoader = getClass().getClassLoader();
        URL resource = classLoader.getResource(fileName);
        if (resource == null) {
            throw new IllegalArgumentException("file is not found!");
        } else {
            this.file = new File(resource.getFile());
        }
    }
}
