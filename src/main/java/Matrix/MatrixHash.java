package Matrix;

import java.util.HashMap;

public class MatrixHash {
    private static HashMap<MatrixType, double[][]> matrixData;

    private static MatrixHash matrixHash;

    public static MatrixHash getMatrixHash() {
        if (matrixHash == null) {
            matrixHash = new MatrixHash();
        }
        return matrixHash;
    }

    private MatrixHash() {
        matrixData = new HashMap<MatrixType, double[][]>();
        matrixData.put(MatrixType.FORWARDS_INCIDENCE, new double[][]{});
        matrixData.put(MatrixType.BACKWARDS_INCIDENCE, new double[][]{});
        matrixData.put(MatrixType.COMBINED_INCIDENCE, new double[][]{});
        matrixData.put(MatrixType.INHIBITION, new double[][]{});
        matrixData.put(MatrixType.MARKING, new double[][]{});
        matrixData.put(MatrixType.ENABLED_TRANSITIONS, new double[][]{});
    }

    public double[][] getMatrixDataType(MatrixType matrixType) {
        return matrixData.get(matrixType);
    }

    public HashMap<MatrixType, double[][]> getMatrixData() {
        return matrixData;
    }

    public void setMatrixData(MatrixType matrixType, double[][] matrix) {
        matrixData.put(matrixType, matrix);
    }
}
