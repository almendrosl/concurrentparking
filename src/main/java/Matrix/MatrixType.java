package Matrix;

public enum MatrixType {
    FORWARDS_INCIDENCE("Forwards incidence matrix I+"), BACKWARDS_INCIDENCE("Backwards incidence matrix I-"),
    COMBINED_INCIDENCE("Combined incidence matrix I"), INHIBITION("Inhibition matrix H"),
    MARKING("Marking"), ENABLED_TRANSITIONS("Enabled transitions");

    private final String name;

    MatrixType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static MatrixType getMatrixType(String name) {
        for(MatrixType tmp: MatrixType.values()) {
            if(tmp.getName().equals(name)) {
                return tmp;
            }
        }
        return null;
    }
}
