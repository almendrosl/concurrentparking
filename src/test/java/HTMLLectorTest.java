import XMLLector.HTMLReader;
import org.testng.annotations.Test;
import org.testng.Assert;

import java.io.IOException;

public class HTMLLectorTest {

    @Test
    public void getHTMLLector_constructor_canRead_exist() throws IOException {
        HTMLReader file = new HTMLReader("PNTPConcurrentes.html");
        Assert.assertTrue(file.getFile().canRead());
        Assert.assertTrue(file.getFile().exists());
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void constructorHTMLLector_throwExeption() {
        new HTMLReader("not_exist.html");
    }

}
