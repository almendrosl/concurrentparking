import Matrix.MatrixHash;
import Matrix.MatrixType;
import XMLLector.HTMLReader;
import XMLLector.MatrixParser;
import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.impl.DenseDoubleMatrix1D;
import cern.colt.matrix.impl.DenseDoubleMatrix2D;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;

public class MatrixParserTest {

    @Test
    public void testMatrixParser()  {
        File file = new HTMLReader("PNTPConcurrentes.html").getFile();
        new MatrixParser(file);

        double[][] mat = MatrixHash.getMatrixHash().getMatrixDataType(MatrixType.ENABLED_TRANSITIONS);

        DenseDoubleMatrix2D ArrayRdPThreads = new DenseDoubleMatrix2D(mat);
        System.out.println(ArrayRdPThreads);
    }

    @Test
    public void testMatrixType()  {
        MatrixType ForwardsMatrix = MatrixType.getMatrixType("Forwards incidence matrix I+");
        Assert.assertEquals(ForwardsMatrix, MatrixType.FORWARDS_INCIDENCE);
    }

    @Test
    public void testMatrixMul()  {
        DenseDoubleMatrix1D sensibilizadasCOLT = new DenseDoubleMatrix1D(new double[]{1, 2, 4, 2});
        DenseDoubleMatrix1D quienesEstanCOLT = new DenseDoubleMatrix1D(new double[]{1, 2, 4, 2});
        DoubleMatrix1D mult = sensibilizadasCOLT.assign(quienesEstanCOLT, cern.jet.math.Functions.mult);
        System.out.println(mult);
    }
}
