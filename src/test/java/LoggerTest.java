import org.junit.Test;
import simulacion.simulacion.Logger;

import java.io.IOException;

public class LoggerTest {

    @Test
    public void testSort() {
        Logger logger = new Logger();

        int disparo1 = 0;
        int disparo2 = 1;
        int disparo3 = 2;

        logger.logDisparo(disparo1);

        try {
            Thread.currentThread().sleep(2000);
        }
        catch (InterruptedException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        logger.logDisparo(disparo3);

        try {
            Thread.currentThread().sleep(2000);
        }
        catch (InterruptedException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        logger.logDisparo(disparo2);

        try {
            Thread.currentThread().sleep(2000);
        }
        catch (InterruptedException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        logger.logDisparo(disparo1);

        try {
            Thread.currentThread().sleep(2000);
        }
        catch (InterruptedException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        logger.logDisparo(disparo3);

        logger.sortLogs();

        try {
            logger.createFile();
        }
        catch (IOException e){
            e.printStackTrace();
        }

        try {
            logger.createFileWithTime();
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }


}