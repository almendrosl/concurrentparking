import Monitor.Politica;
import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.impl.DenseDoubleMatrix2D;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class PoliticaTest {

    private Politica politica;

    @BeforeMethod
    public void setUp() {
        int[] vectorPolitica = {0, 3, 2, 1, 5, 4};
        politica = new Politica(vectorPolitica);
    }

    @AfterMethod
    public void tearDown() {
        politica = null;
    }

    @Test
    public void getMatrizDePrioridades_lenghtSix() {
        double[][] matrixExp = {
                {1, 0, 0, 0, 0, 0},
                {0, 0, 0, 1, 0, 0},
                {0, 0, 1, 0, 0, 0},
                {0, 1, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 1},
                {0, 0, 0, 0, 1, 0}
        };
        final DoubleMatrix2D expectedMatrix = new DenseDoubleMatrix2D(matrixExp);

        final DoubleMatrix2D actualMatrix = politica.getMatrizDePrioridades();

        Assert.assertEquals(actualMatrix, expectedMatrix);
    }

    @Test
    public void setPolitica_lenghtFive() {
        double[][] matrixExp = {
                {0, 0, 0, 0, 1, 0},
                {0, 0, 0, 1, 0, 0},
                {0, 0, 1, 0, 0, 0},
                {0, 1, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 1},
                {1, 0, 0, 0, 0, 0},
        };
        final DoubleMatrix2D expectedMatrix = new DenseDoubleMatrix2D(matrixExp);

        int[] vectorPolitica = {4, 3, 2, 1, 5, 0};
        politica.setPolitica(vectorPolitica);

        final DoubleMatrix2D actualMatrix = politica.getMatrizDePrioridades();

        Assert.assertEquals(actualMatrix, expectedMatrix);
    }

    @Test
    public void getMaximaPrioridad_lenghtFive_ProrityZero() {
        double expectedPriority = 0;
        double[] matrixExp = {1, 0, 0, 1, 0, 0};

        double actualPriority = politica.getMaximaPrioridad(matrixExp);

        Assert.assertEquals(actualPriority, expectedPriority);
    }

    @Test
    public void getMaximaPrioridad_lenghtFive_ProrityThree() {
        double expectedPriority = 3;
        double[] matrixExp = {0, 1, 0, 1, 1, 1};

        double actualPriority = politica.getMaximaPrioridad(matrixExp);

        Assert.assertEquals(actualPriority, expectedPriority);
    }

    @Test
    public void getMaximaPrioridadArray_lenghtFive_ProrityZero() {
        double[] expectedPriority = {1, 0, 0, 0, 0, 0};
        double[] matrixExp = {1, 0, 0, 1, 0, 0};

        double[] actualPriority = politica.getMaximaPrioridadArray(matrixExp);

        Assert.assertEquals(actualPriority, expectedPriority);
    }

    @Test
    public void getMaximaPrioridadArray_lenghtFive_ProrityThree() {
        double[] expectedPriority = {0, 0, 0, 1, 0, 0};
        double[] matrixExp = {0, 1, 0, 1, 1, 1};

        double[] actualPriority = politica.getMaximaPrioridadArray(matrixExp);

        Assert.assertEquals(actualPriority, expectedPriority);
    }
}
