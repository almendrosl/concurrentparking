import RdP.InvarianteP;
import RdP.VectorDeEstado;
import RdP.VectorSensibilizadas;
import Matrix.MatrixHash;
import Matrix.MatrixType;
import XMLLector.HTMLReader;
import XMLLector.MatrixParser;
import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.impl.DenseDoubleMatrix1D;
import cern.colt.matrix.impl.DenseDoubleMatrix2D;
import cern.jet.math.Functions;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.File;
import java.util.List;

public class PetriTest {

    private double[][] ventanas;
    @BeforeMethod
    public void setUp() {
        File file = new HTMLReader("PetriTest.html").getFile();
        new MatrixParser(file);
        int transiciones = MatrixHash.getMatrixHash().getMatrixDataType(MatrixType.COMBINED_INCIDENCE)[0].length;
        ventanas = new double[transiciones][2];
    }

    @Test
    public void transicionInmediataTest(){
        System.out.println("Ventanas de tiempo: " + new DenseDoubleMatrix2D(ventanas));

        VectorSensibilizadas sensibilizadas = new VectorSensibilizadas(ventanas);

        double[] disparo = {1,0,0,0,0,0,0,0,0};
        boolean k = sensibilizadas.estaSensibilizado(disparo);

        Assert.assertTrue(k);

    }

    @Test
    public void antesVentanaTest(){
        ventanas[1][0] = 4000;
        ventanas[1][1] = 6000;
        VectorSensibilizadas sensibilizadas = new VectorSensibilizadas(ventanas);
        double[] disparo = {0,1,0,0,0,0,0,0,0};

        boolean k = sensibilizadas.estaSensibilizado(disparo);

        Assert.assertTrue(k);
    }

    @Test
    public void enVentanaTest(){
        ventanas[1][0] = 0;
        ventanas[1][1] = 6000;
        VectorSensibilizadas sensibilizadas = new VectorSensibilizadas(ventanas);
        double[] disparo = {0,1,0,0,0,0,0,0,0};

        boolean k = sensibilizadas.estaSensibilizado(disparo);

        Assert.assertTrue(k);
    }

    @Test
    public void despuesVentanaTest(){
        ventanas[1][0] = 0;
        ventanas[1][1] = 1;

        VectorSensibilizadas sensibilizadas = new VectorSensibilizadas(ventanas);
        double[] disparo = {0,1,0,0,0,0,0,0,0};

        try {
            Thread.currentThread().sleep(2000);
        }
                catch (InterruptedException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        boolean k = sensibilizadas.estaSensibilizado(disparo);

        Assert.assertFalse(k);
    }

    @Test
    public void getSensibilizadas(){

        DenseDoubleMatrix2D incidencia = new DenseDoubleMatrix2D(MatrixHash.getMatrixHash().getMatrixDataType(MatrixType.COMBINED_INCIDENCE));
        DenseDoubleMatrix2D inhibicion = new DenseDoubleMatrix2D(MatrixHash.getMatrixHash().getMatrixDataType(MatrixType.INHIBITION));

        DenseDoubleMatrix1D marcado = new DenseDoubleMatrix1D(new double[]{0,2,0,0,0,0,0,1});

        System.out.println("Incidencia: " + incidencia + "\n");
        System.out.println("Inhibicion: " + inhibicion + "\n");
        System.out.println("Marcado Actual: " + marcado + "\n");


        double[] vectorE = new double[incidencia.viewRow(0).toArray().length];

        for(int i = 0; i < incidencia.viewRow(0).toArray().length; i++){
            DoubleMatrix1D columna = incidencia.viewColumn(i);
            DoubleMatrix1D result = columna.assign(marcado, Functions.plus);
            vectorE[i] = result.aggregate(Functions.mult, cern.jet.math.Functions.greater(-1));
        }

        DenseDoubleMatrix1D E = new DenseDoubleMatrix1D(vectorE);

        DenseDoubleMatrix1D ceroVector = marcado;

        ceroVector.assign(cern.jet.math.Functions.greater(0));

        DenseDoubleMatrix1D B = new DenseDoubleMatrix1D(E.size());

        inhibicion.zMult(ceroVector,B,1,1,true);
        B.assign(cern.jet.math.Functions.equals(0));

        DoubleMatrix1D Ex = E.assign(B, Functions.mult);

        //double[] expected = {1,1,0,1,0,0,1,0,0};
        double[] expected = {1,0,0,1,1,0,1,0,0};

        Assert.assertEquals(Ex.toArray(), expected);
    }

}